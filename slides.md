---
title: "SiC - Signierte Container"
author: delet0r
date: 2023-09-01
---

## Agenda

1. Einleitung
2. Lösungsansatz
3. Ergebnisse

::: notes
  - Hallo wir sind SiC wir wollen digitale Verwaltung endlich wieder langweilig machen
- Einleitung: primer zu Kontext
- Lösungsansatz: Kurze Einführung in unsere Lösung und die verwendete Technologie
- Zusammenfassung von Ergebnissen und den Erkenntnissen die wir daraus gezogen haben
:::

# Einleitung

## Wilder Westen

::: {.container}
:::::: {.col}
- Auf irgendwelcher Leute Computer<br>läuft irgendwelche Software <br>und tut irgendwelche Dinge
- Keine festen Strukturen /<br>ständig ändernde Strukturen
- Langfristige Lösungen benötigt
:::::::
:::::: {.col}
![Wilder Westen Lagerplatz](050_Projects/Vorträge/Prototype%20Fund%20Demo-Day%20Talk/attachments/wild_west.png){style='padding: 1px'}
::::::
:::


::: notes
- Moderne Software Entwicklung ist ein bisschen wie der Wilde Westen
- Wir haben Cloud
- Also auf irgendwelcher Leute Computer läuft irgendwelche Software und tut irgendwelche Dinge
- Und alle machen mehr oder weniger was sie wollen
- So etwas ist für eine Verwaltung denkbar ungeeignet
- Eine Verwaltung und auch eine digitale Verwaltung braucht Stabilität und Sicherheit 
- Nicht nur für sich selbst, sondern auch für ihre Nutzer\*innen
- Das meinen wir, wenn wir davon sprechen, dass Digitale Verwaltung endlich wieder langweilig werden soll
:::

## Schlangenölverkäufer


::: {.container}
:::::: {.col}
- Vertrauenswürdigkeit ist wichtig
- Für verwendete Software (Abhängigkeiten)
- Für Nutzer\*innen eines Dienstes 
:::::::

:::::: {.col}
![Schlangenölwerbung](050_Projects/Vorträge/Prototype%20Fund%20Demo-Day%20Talk/attachments/snake_oil.png){width=35%}
::::::
:::

::: notes
- In einer solchen Umgebung gibt es auch viele bösartige Akteure
- Daher ist es wichtig, wenn ich einer Entität, wie z.B. einer Verwaltung vertraue
- sicherstellen zu können, dass es sich um diese handelt und das die Daten die wir austauschen auch wirklich uns kommen
- Sog. Authentizität und Integrität
- Wieso das wichtig ist können wir an folgendem Beispiel sehen 
:::

## Beispiel

![Fortinet Artikel Screenshot @lee2023supply](050_Projects/Vorträge/Prototype%20Fund%20Demo-Day%20Talk/attachments/python_supply_chain_attack.png){width=60%}

::: notes
- Hier sehen wir einen Blogeintrag des Sicherheitsunternehmen Fortinet 
- In dem Artikel beschreiben sie, dass sie alleine zwischen Februar und März diesen Jahres 
- 60 Pakete im Python Paket Manager gefunden die bösartigen Code enthalten
- Hier ist gut zu sehen wie wichtig es für die Digitalisierung der Verwaltung ist sich im Vorhinein Gedanken über den Umgang mit solchen Problemen zu machen
:::

## Authentizität und Integrität in der Verwaltung


::: {.container}
:::::: {.col}
- Siegel als Unterschrift der Verwaltung- Bestätigt Authentizität 
- Siegel als Beweis des Vertrauensankers (die Institution)
- Siegel auf dem Dokument als Nachweis der Integrität
:::::::

:::::: {.col}
  
![Siegel des/der Innensenator\*in von Berlin](050_Projects/Vorträge/Prototype%20Fund%20Demo-Day%20Talk/attachments/landessiegel_berlin.png){style='padding: 1px'}
::::::
:::

::: notes
- In der Analogen Welt hat die Verwaltung das von Authentizität und Integrität schon lange mit Siegeln gelöst
- z.B. werden Verkündete Gesetze werden gesiegelt, aber auch viele andere offizielle Dokumente wie Urkunden oder Verträge
- Konzeptuell bestätigt bestätigt eine Vertraute Instanz die Authentizität eines Dokuments durch ein Siegel
- Gleichzeitig Bestätigt das Siegel den Inhalts des Dokuments (der Aufgrund seiner Physikalität schwer geändert werden kann)
:::

## Authentizität und Integrität in Software

- Siegelung grundsätzlich mit Kryptographischen Signaturen möglich
- Im digitalen stärkerer Fokus auf Beweisbarkeit
- Anforderung verkompliziert durch dezentrale Entwicklung in Open Source Software

::: notes
- Aufgrund der mangelnden Physikalität lässt sich der Analoge Ansatz nicht einfach ins digitale übertragen
- Bei Analogen Verfahren ist die Sicherheit weniger wichtig als der Eigentliche Prozess der Ausgeführt wird
- Beim technischen Äquivalent der des Siegels - der Signatur - ist das anders
- Digitale Informationen zu ändern und zu reproduzieren ist trivial
- Daher ist die Signatur - im Sinne eines digitalen Siegels - ein technisches Verfahren was mathematische Beiweise verwendet um Authentizität und Integrität zu beweisen und nicht einfach nur anzunehmen
- Speziell in modernen Open Source Software ist das wichtig
- Da diese meist auf kompartmentalisierten Teilen basieren die von unterschiedlichen Teams entwickelt werden
- Wie wir am Beispiel vorhin schon gesehen haben stellt sich auch außerhalb der Verwaltung heraus, dass es schwer sein kann diese Eigenschaften zu garantieren
- Ein weiter Grund dafür, dass solche Betrachtung im Vorhinein getätigt werden um diese beim design von Lösungen ganzheitlich mit einzubeziehen
- Vor allem weil Verwaltung keine grüne Wiese ist, sondern aus existierenden Strukturen heraus weiterentwickelt werden muss
:::

# Lösungsansatz

## Lösungsansatz

- Derzeit kein etablierter Lösungsansatz
- Verschiedene Projekte entwickeln Lösungsansätze
- SiC untersuchen wie sich Sigstore in ein Verwaltungsfachverfahren integrieren lässt

::: notes
- Mit diesem Kontext möchte ich euch jetzt unseren Ansatz vorstellen
- Da es derzeit für etabliertes Verfahren zum signieren (also siegeln) von Software im Cloudbereich gibt
- haben wir uns einen vielversprechenden Kandidaten - mit Namen Sigstore - ausgesucht
- Ziel des Projekts war es die Integrierbarkeit von Sigstore in ein Fachverfahren zu untersuchen
- Das heiß:  Eine autonome Instanz aufzusetzen und die notwendige Prozesse die Infrastruktur des Fachverfahrens zu integrieren 
:::

## Sigstore

- Vergleichbar mit HTTPS
- System um Integrität und Authentizität über Distributionsplattformen hinweg technisch sicherzustellen
- Basiert auf freien standardisierten Technologien und Formaten
- Spezialisiert auf gängige Cloud/Microservice-Architekturen

::: notes
- Aber was ist Sigstore?
- Vergleichbar mit HTTPS,
  - HTTPS soll garantieren, dass Authentizität und Integrität für eine Website sicherstellen
  - Sigstore soll Authentizität und Integrität für Software Pakete (speziell Container) sicherstellen, mit der z.B. eine Verwaltungsorgan eine digitalisiertes Fachverfahren über eine Website zur Verfügung stellt
- 
:::

## Sigstore Funktion

- *Trust Root*: Sichert kryptographisches Material
- (*Identity Provider*: Externes System dem vertraut wird und das Identitätsüberprüfung ermöglicht)
- *Fulcio*: Identitätsüberprüfung und Zertifikat zum signieren von Software
- *Rekor*: Öffentlicher Signaturspeicher

::: notes
- Um euch ein Gefühl zu geben was Sigstore eigentlich macht
- hier einmal alle Komponenten und deren Aufgaben
- *Trust Root*: Sichert kryptographisches Material
- (*Identity Provider*: Externes System dem vertraut wird und das Identitätsüberprüfung ermöglicht)
- *Fulcio*: Identitätsüberprüfung und Zertifikat zum signieren von Software
- *Rekor*: Öffentlicher Signaturspeicher
:::

# Ergebnis

## Ergebnis

- Vollständige Umsetzung der Zielsetzung wurde nicht geschafft
- Komplexität der Lösung unterschätzt
- Viele Baustellen die sich gegenseitig geblockt haben

::: notes
- Leider haben wir es nicht geschafft die Zielsetzung vollständig Umzusetzen
- Das hatte unterschiedliche Gründe aber die Primären waren:
  - Das wir initial die Komplexität der Lösung unterschätz haben
    - wir hatten vorher noch nicht tiefgehend mit Sigstor gearbeitet und eine quirks haben uns sehr viel Zeit gekostet
  - Dazu kamen unterschiedliche Baustellen, die sich gegenseitig geblockt haben, sowie allgemeine Zeitprobleme
- Trotzdem ist es uns wichtig die Erkenntnisse die gehabt haben hier nochmal festzuhalten 
:::

## Erkentnisse

- Trotz Misserfolg sinnvoller und notwendiger Ansatz
- Keine Plug-In Lösung
- Setzt eine gewisse Teamgröße und Spezialisierung voraus
- Kontinuierliches commitment

::: notes
- Wir sind weiterhin der Überzeugung, dass der Umgang mit diesem Thema notwendig ist um eine Erfolgreiche und langfristig erfolgreiche Digitalisierung der Verwaltung vorzunehmen
- Was unsere Schwierigkeiten mit der Lösung aber auch gezeigt haben, dass das keine Triviale Plug-in Lösung ist
- Eine erfolgreiche Integration und Nutzung setzt eine gewisse Teamgröße und Spezialisierung voraus für einen stabilen Betrieb voraus
- Sigstore ist eine komplexe Lösung für ein komplexes Problem
- Glücklicherweise ist die Architektur von Sigstore optimal für die Nutzung in der Verwaltung, da eine vom Bund oder Land aufgesetzte Instanz von allen Verwaltungen genutzt werden kann
- Diese kann gleichzeitig von Außen überprüft werden ohne, dass zusätzliche Drittanbieter nötig werden
- und damit möchte ich mich für eure Aufmerksamkeit bedanken
:::

# Danke für eure Aufmerksamkeit!

#
::: {#refs .allowframebreaks}
:::
